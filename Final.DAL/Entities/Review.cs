﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.Entities
{
    public class Review : IEntity
    {
        public int Id { get; set; }
        public DateTime DatePublished { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
    }
}
