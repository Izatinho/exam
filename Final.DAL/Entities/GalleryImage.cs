﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.Entities
{
    public class GalleryImage : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public int CafeId { get; set; }
        public Cafe Cafe { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
