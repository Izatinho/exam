﻿using System.Collections.Generic;

namespace Final.DAL.Entities
{
    public class Cafe : IEntity
    {
        public int Id { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public byte[] Image { get; set; }
        public CafeImageType CafeImageType { get; set; }
        
        public ICollection<Review> Reviews { get; set; }
        public ICollection<GalleryImage> GalleryImages { get; set; }
    }
}
