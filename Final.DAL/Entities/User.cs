﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Review> Reviews { get; set; }
        public ICollection<Cafe> Caves { get; set; }

    }
}
