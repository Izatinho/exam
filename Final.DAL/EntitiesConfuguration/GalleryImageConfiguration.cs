﻿using Final.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.EntitiesConfuguration
{
    public class GalleryImageConfiguration : BaseEntityConfiguration<GalleryImage>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<GalleryImage> builder)
        {
            builder
                .HasOne(x => x.Cafe)
                .WithMany(x => x.GalleryImages)
                .HasForeignKey(x => x.CafeId)
                .IsRequired();
        }
    }
}
