﻿using Final.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.EntitiesConfuguration
{
    public class ReviewConfiguration : BaseEntityConfiguration<Review>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Review> builder)
        {
            base.ConfigureForeignKeys(builder);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Review> builder)
        {
            builder
                .Property(x => x.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(x => x.DatePublished)
                .IsRequired();
               
        }
    }
}
