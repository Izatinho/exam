﻿using Final.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.EntitiesConfuguration
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Cafe> CafeConfiguration { get; }
        IEntityConfiguration<Review> ReviewConfiguration { get; }
    }
}
