﻿using Final.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.EntitiesConfuguration
{
    public class CafeConfiguration : BaseEntityConfiguration<Cafe>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Cafe> builder)
        {
            base.ConfigureForeignKeys(builder);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Cafe> builder)
        {
            builder
                .Property(x => x.Title)
                .HasMaxLength(50)
                .IsRequired();
                        
        }
    }
}
