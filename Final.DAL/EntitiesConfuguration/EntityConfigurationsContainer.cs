﻿using Final.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.EntitiesConfuguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {

        public IEntityConfiguration<Cafe> CafeConfiguration { get; }

        public IEntityConfiguration<Review> ReviewConfiguration { get; }
        public EntityConfigurationsContainer()
        {
            CafeConfiguration = new CafeConfiguration();
            ReviewConfiguration = new ReviewConfiguration();
        }

    }
}
