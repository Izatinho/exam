﻿

namespace Final.DAL.Context
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
