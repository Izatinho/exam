﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Final.DAL.Migrations
{
    public partial class AddingUserIdFieldToCafeEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Caves",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Caves",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
