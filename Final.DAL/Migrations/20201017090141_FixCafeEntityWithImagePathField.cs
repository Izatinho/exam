﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Final.DAL.Migrations
{
    public partial class FixCafeEntityWithImagePathField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "Caves",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "Caves");
        }
    }
}
