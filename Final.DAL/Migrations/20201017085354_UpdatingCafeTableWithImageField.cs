﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Final.DAL.Migrations
{
    public partial class UpdatingCafeTableWithImageField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CafeImageType",
                table: "Caves",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CafeImageType",
                table: "Caves");
        }
    }
}
