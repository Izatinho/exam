﻿using Final.DAL.Context;
using Final.DAL.Entities;
using Final.DAL.Repositories.Contracts;

namespace Final.DAL.Repositories
{
    public class GalleryImageRepository : Repository<GalleryImage>, IGalleryImageRepository
    {
        public GalleryImageRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.GalleryImages;
        }
    }
}
