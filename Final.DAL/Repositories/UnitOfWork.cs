﻿using Final.DAL.Context;
using Final.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;
        public ICafeRepository Caves { get; }
        public IReviewRepository Reviews { get; }
        public IGalleryImageRepository GalleryImages { get; }

        
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Reviews = new ReviewRepository(context);
            Caves = new CafeRepository(context);
            GalleryImages = new GalleryImageRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
