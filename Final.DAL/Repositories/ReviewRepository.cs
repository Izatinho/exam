﻿using Final.DAL.Context;
using Final.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Final.DAL.Repositories
{
    public class ReviewRepository : Repository<Review>, IReviewRepository
    {
        public ReviewRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Reviews;
        }

        public IEnumerable<Review> GetAllWithAuthors()
        {
            return entities
                .Include(x => x.Author)
                .ToList();
        }
    }
}
