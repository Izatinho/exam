﻿using Final.DAL.Context;
using Final.DAL.Entities;
using Final.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Final.DAL.Repositories
{
    public class CafeRepository : Repository<Cafe>, ICafeRepository
    {
        public CafeRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Caves; 
        }

        public Cafe GetAllWithReview(int cafeId)
        {
            return entities
                .Include(x => x.Reviews)
                .ThenInclude(x => x.Author)
                .FirstOrDefault(x => x.Id == cafeId);
        }

        public IEnumerable<Cafe> GetAllWithReviews()
        {
            return
                entities
                .Include(x => x.Reviews)
                .ToList();
        }
    }
}
