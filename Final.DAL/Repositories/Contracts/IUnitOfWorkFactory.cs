﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
