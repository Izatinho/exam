﻿using Final.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.Repositories.Contracts
{
    public interface ICafeRepository : IRepository<Cafe>
    {
        IEnumerable<Cafe> GetAllWithReviews();
        Cafe GetAllWithReview(int cafeId);
    }
}
