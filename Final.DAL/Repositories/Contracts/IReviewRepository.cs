﻿using Final.DAL.Entities;
using Final.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Final.DAL.Repositories
{
    public interface IReviewRepository : IRepository<Review>
    {
        IEnumerable<Review> GetAllWithAuthors();
    }
}
