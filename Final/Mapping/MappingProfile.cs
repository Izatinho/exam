﻿using AutoMapper;
using Final.DAL.Entities;
using Final.Models;
using Final.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateCafeToCafeModelMap();
            CreateReviewToReviewModelMap();
            CreateCafeCreateModelToCafe();
            CreateAddReviewRequestModelToReviewMap();
        }
        public void CreateCafeToCafeModelMap()
        {
            CreateMap<Cafe, CafeModel>()
                .ForMember(x => x.Name,
                src => src.MapFrom(x => x.Title))
                .ForMember(x => x.Images,
                src => src.MapFrom(x => x.GalleryImages));          

        }
        public void CreateReviewToReviewModelMap()
        {
            CreateMap<Review, ReviewModel>()
                .ForMember(x => x.AuthorName,
                src => src.MapFrom(x => x.Author))
                .ForMember(x => x.CreatedOn,
                src => src.MapFrom(x => x.DatePublished.ToString("D")));
                
        }
        public void CreateCafeCreateModelToCafe()
        {
            CreateMap<CafeCreateModel, Cafe>()
                .ForMember(x => x.Title,
                src => src.MapFrom(x => x.Title))
                .ForMember(x => x.Image,
                src => src.Ignore());
               
        }

        public void CreateAddReviewRequestModelToReviewMap()
        {
            CreateMap<AddReviewRequestModel, Review>()
                .ForMember(x => x.Content,
                src => src.MapFrom(x => x.Review));
        }
    }

    
}
