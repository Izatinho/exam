﻿using Final.DAL.Entities;
using Final.Mapping;
using Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Services.Contracts
{
    public interface ICafeService
    {
        List<CafeModel> GetAllCaves(CafeIndexModel model);
        CafeCreateModel GetCreateModel();
        void CreateCafe(CafeCreateModel model, int id);
        CafeModel GetCafeById(int cafeId);
        ReviewModel AddReview(AddReviewRequestModel model, User currentUser);
    }
}
