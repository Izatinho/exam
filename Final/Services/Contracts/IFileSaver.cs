﻿using Final.DAL.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Services.Contracts
{
    public interface IFileSaver
    {
        void SaveFile(Cafe cafe, IFormFile formFile);
    }
}
