﻿using Final.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Services.Contracts
{
    public interface IReviewService
    {
        IEnumerable<ReviewModel> GetAllReviews();
    }
}
