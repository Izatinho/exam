﻿using Final.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Services
{
    public static class CafeServiceExtensions
    {
        public static IEnumerable<Cafe> BySearchKey(this IEnumerable<Cafe> caves, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                caves = caves.Where(x => x.Title.Contains(searchKey));
            return caves;
        }

        
    }
}
