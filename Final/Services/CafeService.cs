﻿using AutoMapper;
using Final.DAL.Entities;
using Final.DAL.Repositories.Contracts;
using Final.Mapping;
using Final.Models;
using Final.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Services
{
    public class CafeService : ICafeService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFileSaver _fileSaver;
        private readonly DbFilesSaver _dbFileSaver;

        public CafeService(IUnitOfWorkFactory unitOfWorkFactory, IFileSaver fileSaver, DbFilesSaver dbFilesSaver)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _fileSaver = fileSaver;
            _dbFileSaver = dbFilesSaver;
        }

        public CafeCreateModel GetCreateModel()
        {
            return new CafeCreateModel();
            
        }

        public List<CafeModel> GetAllCaves(CafeIndexModel model)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var caves = uow.Caves.GetAllWithReviews();

                caves = caves
                    .BySearchKey(model.SearchKey);

                int pageSize = 3;
                int count = caves.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                caves = caves.Skip((page - 1) * pageSize).Take(pageSize);
                model.PagingModel = new PagingModel(count, page, pageSize);
                model.Page = page;


                var models = Mapper.Map<List<CafeModel>>(caves.ToList());


                models.OrderBy(x => x.Name);

                return models;
            }
        }

        public void CreateCafe(CafeCreateModel model, int currentUserId)
        {
            using(var uow = _unitOfWorkFactory.Create())
            {
                var cafe = Mapper.Map<Cafe>(model);
                cafe.UserId = currentUserId;
                _fileSaver.SaveFile(cafe, model.Image);
                uow.Caves.Create(cafe);
                    
            }
        }

        public CafeModel GetCafeById(int cafeId)
        {
            using(var uow = _unitOfWorkFactory.Create())
            {
                var cafe = uow.Caves.GetAllWithReview(cafeId);
                return Mapper.Map<CafeModel>(cafe);
            }
        }

        public ReviewModel AddReview(AddReviewRequestModel model, User currentUser)
        {
            using(var uow = _unitOfWorkFactory.Create())
            {
                var review = Mapper.Map<Review>(model);
                review.AuthorId = currentUser.Id;
                review.DatePublished = DateTime.Now;
                uow.Reviews.CreateAsync(review);
                review.Author = currentUser;
                return Mapper.Map<ReviewModel>(review);
            }
        }
    }
}
