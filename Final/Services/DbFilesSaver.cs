﻿using Final.DAL.Entities;
using Final.Services.Contracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Services
{
    public class DbFilesSaver : IFileSaver
    {
        public void SaveFile(Cafe cafe, IFormFile formFile)
        {
            cafe.Image = GetImageBytes(formFile);
            cafe.CafeImageType = CafeImageType.Db;
        }

        public byte[] GetImageBytes(IFormFile formFile)
        {
            using (var binaryReader = new BinaryReader(formFile.OpenReadStream()))
            {
                return binaryReader.ReadBytes((int)formFile.Length);
            }
        }
    }
}
