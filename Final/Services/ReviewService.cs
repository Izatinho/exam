﻿using AutoMapper;
using Final.DAL.Repositories.Contracts;
using Final.Mapping;
using Final.Services.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace Final.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ReviewService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IEnumerable<ReviewModel> GetAllReviews()
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var reviews = uow.Reviews.GetAllWithAuthors();
                var models = Mapper.Map<List<ReviewModel>>(reviews.ToList());
                return models;
            }
        }

        
    }
}