﻿namespace Final.Models
{
    public class GalleryImageModel
    {
        public string Name { get; set; }
        public byte[] Image { get; set; }
    }
}