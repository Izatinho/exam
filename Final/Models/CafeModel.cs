﻿using Final.DAL.Entities;
using Final.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Models
{
    public class CafeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public string ImagePath { get; set; }
        public CafeImageType CafeImageType { get; set; }
        public int Rating { get; set; }
        public List<ReviewModel> Reviews { get; set; }
        public List<GalleryImageModel> Images { get; set; }
    }
}
