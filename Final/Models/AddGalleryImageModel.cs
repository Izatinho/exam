﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Models
{
    public class AddGalleryImageModel
    {
        [Required]
        public int CafeId { get; set; }

        public IFormFileCollection Images { get; set; }
    }
}
