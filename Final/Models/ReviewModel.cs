﻿using System.ComponentModel.DataAnnotations;

namespace Final.Mapping
{
    public class ReviewModel
    {
        public int Id { get; set; }
        [Display(Name = "Date of published")]
        public string CreatedOn { get; set; }
        [Display(Name = "Author")]
        public string AuthorName { get; set; }
        [Display(Name = "Review")]
        public string Content { get; set; }
    }
}