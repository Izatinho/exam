﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final.Models
{
    public class CafeIndexModel
    {
        public string Title { get; set; }
        public string SearchKey { get; set; }
        public List<CafeModel> Caves { get; set; }
        public int? Page { get; set; }
        public PagingModel PagingModel { get; set; }
    }
}
