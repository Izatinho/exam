﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
namespace Final.Models
{
    public class AddReviewRequestModel
    {
        [Required]
        [JsonProperty(propertyName: "comment")]
        public string Review { get; set; }
        [Required]
        [JsonProperty(propertyName: "cafeId")]
        public int CafeId { get; set; }
    }
}
