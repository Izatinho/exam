﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Final.DAL.Entities;
using Final.Models;
using Final.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Final.Controllers
{
    public class CafeController : Controller
    {
        private readonly ICafeService _cafeService;
        private readonly UserManager<User> _userManager;

        public CafeController(ICafeService cafeService, UserManager<User> userManager)
        {
            _cafeService = cafeService;
            _userManager = userManager;
        }

        public IActionResult Index(CafeIndexModel model)
        {
            try
            {

                var models = _cafeService.GetAllCaves(model);
                model.Caves = models;
                return View(model);

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        [Authorize]
        public IActionResult Create()
        {
            var model = _cafeService.GetCreateModel();
            return View(model);
        }

        [Authorize]
        [HttpPost]

        public async Task<IActionResult> CreateCafe(CafeCreateModel model)
        {
            try
            {
                User user = await _userManager.GetUserAsync(User);
                _cafeService.CreateCafe(model, user.Id);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        [Authorize]
        [HttpGet]
        public IActionResult Details(int cafeId)
        {
            var cafeModel = _cafeService.GetCafeById(cafeId);
            return View(cafeModel);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddReview(AddReviewRequestModel model)
        {
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);
                var reviewModel = _cafeService.AddReview(model, currentUser);
                return Ok(reviewModel);
            }
            catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

    }
}
